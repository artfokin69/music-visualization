import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import gsap from 'gsap'
import * as dat from 'dat.gui';
import djazAudio from './assets/audio.mp3';
import mandoAudio from './assets/audio2.mp3';

const audio = document.getElementById('audio') as HTMLAudioElement;
audio.addEventListener('play',()=>audioCtx.resume(), {once:true})
const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
const analyser = audioCtx.createAnalyser();
const gainNode = audioCtx.createGain();
const source = audioCtx.createMediaElementSource(audio);
source.connect(analyser);
analyser.connect(gainNode)
gainNode.connect(audioCtx.destination)
analyser.fftSize = 512;
let bufferLength = analyser.frequencyBinCount;
let frequencyData = new Uint8Array(bufferLength);

const {CONTAINER, CAMERA, SCENE, RENDERER, LIGHT} = initTHREE();
const {CLOCK, CONTROLS} = addSceneHelpers();
let {SPHERES_GROUP, SPHERES_DEFAULT_Y} = createSpheres(bufferLength);
let RECT_RADIUS = Math.abs(SPHERES_GROUP.position.z);
addListeners();
animate();
initControls();

function initTHREE(){
  const CONTAINER = document.getElementById('container');

  const CAMERA = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 1000 );
  CAMERA.position.x = 102
  CAMERA.position.y = 62
  CAMERA.position.z = 104
  
  CAMERA.lookAt( 0, 0, 0 );

  const SCENE = new THREE.Scene();
  SCENE.background = new THREE.Color( 0x9088d4 );

  const LIGHT = new THREE.PointLight( 0xffffff, bufferLength / 128 );
	SCENE.add( LIGHT )
	LIGHT.add( new THREE.Mesh( new THREE.SphereGeometry( bufferLength / 256, 32, 32 ), new THREE.MeshBasicMaterial( { color: 0xffffff } ) ) );

  const canvas = document.createElement( 'canvas' );
  const context = canvas.getContext( 'webgl', { alpha: false } );
  
  const RENDERER = new THREE.WebGLRenderer( { canvas, context, antialias: true } );

  RENDERER.setPixelRatio( window.devicePixelRatio );
  RENDERER.setSize( window.innerWidth, window.innerHeight );
  RENDERER.outputEncoding = THREE.sRGBEncoding;
  RENDERER.setClearColor( 0xffffff);

  CONTAINER.appendChild( RENDERER.domElement );

  return {
    CONTAINER,
    SCENE,
    CAMERA,
    RENDERER,
    LIGHT,
  }
}
function animate(){
  const time = CLOCK.getElapsedTime();
  requestAnimationFrame( animate );
  analyser.getByteFrequencyData(frequencyData);
  frequencyData.forEach((frequency, index)=>{
    const sphere = SPHERES_GROUP.children[index];
    const defaultY = SPHERES_DEFAULT_Y[index];
    sphere.position.y = defaultY + frequency / 255 * 10;
  });
  if(CONTROLS){
    CONTROLS.update();
  }
  
  LIGHT.position.x = Math.sin( time * 2 ) * RECT_RADIUS / 2;
  LIGHT.position.z = Math.cos( time ) * RECT_RADIUS / 2;
  
  LIGHT.position.y = 20 + Math.cos( time * 1.5 ) * 5;

  RENDERER.render( SCENE, CAMERA );
}
function createSpheres(count: number){
  const SPHERES_GROUP = new THREE.Group();
  const SPHERES_DEFAULT_Y:number[] = [];
  const shperes: THREE.Mesh[] = [];
  const rowCount = Math.floor(Math.sqrt(count));
  const colCount = Math.ceil(count / rowCount)
  for(let i=0; i < count; i++){
    let sphere = addSphere(i);
    const r = sphere.geometry.parameters.radius;
    const rowIndex = i % rowCount;
    const colIndex = Math.trunc(i / rowCount);
    sphere.position.x = r + rowIndex * 2 * r + rowIndex * 2;
    sphere.position.z = colIndex * 2 * r + colIndex * 2;
    SPHERES_DEFAULT_Y.push(Math.random() * 5);
    SPHERES_DEFAULT_Y.push(0);
    shperes.push(sphere);
    SPHERES_GROUP.add(sphere)
    if(i === rowCount * Math.floor(colCount / 2) +  Math.floor(rowCount / 2)){
      sphere.material.color = new THREE.Color(0x000000);
    }
  }
  const centerSphereIndex = rowCount * Math.floor(colCount / 2) +  Math.floor(rowCount / 2);
  const centerSphere = SPHERES_GROUP.children[centerSphereIndex];
  
  SPHERES_GROUP.translateX(-centerSphere.position.x)
  SPHERES_GROUP.translateZ(-centerSphere.position.z)
  SCENE.add(SPHERES_GROUP);
  return {SPHERES_GROUP, SPHERES_DEFAULT_Y}
}
function addSphere(index: number){
  const geometry = new THREE.SphereGeometry( 1, 32, 32 );
  const material = new THREE.MeshPhongMaterial( {color: Math.random() * 0xffffff} );
  const sphere = new THREE.Mesh( geometry, material );
  return sphere;
}
function addListeners(){
  window.addEventListener('resize', ()=>{
		CAMERA.aspect = window.innerWidth / window.innerHeight;
		CAMERA.updateProjectionMatrix();
		RENDERER.setSize( window.innerWidth, window.innerHeight );
  })
}
function addSceneHelpers(){
  const CLOCK = new THREE.Clock();
  // const axesHelper = new THREE.AxesHelper( 10 );
  // SCENE.add(axesHelper);
  const CONTROLS = new OrbitControls( CAMERA, RENDERER.domElement );
  return {
    CLOCK, 
    CONTROLS
  }
}
function initControls(){
  document.getElementById('getCameraPosition').addEventListener('click',()=>{
    console.log(CAMERA)
  })
}

const gui = new dat.GUI({autoPlace: true});
const soundController = gui.add({sound: 'mando'}, 'sound', {
  'mando':mandoAudio,
  'djazz':djazAudio
})
soundController.onChange(audioSrc=>audio.src = audioSrc)

const countController = gui.add({count: 512}, 'count', new Array(6).fill(0).map((v,i)=>Math.pow(2,i+7)))
countController.onChange(count=>{
  console.log(count)
  analyser.fftSize = count;
  bufferLength = analyser.frequencyBinCount;
  frequencyData = new Uint8Array(bufferLength);
  let spheresInfo = createSpheres(bufferLength);
  SPHERES_GROUP = spheresInfo.SPHERES_GROUP;
  SPHERES_DEFAULT_Y = spheresInfo.SPHERES_DEFAULT_Y;
  RECT_RADIUS = Math.abs(SPHERES_GROUP.position.z);
})