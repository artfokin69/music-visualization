const audio = document.getElementById('audio');
const canvas = document.querySelector('canvas')
const ctx = canvas.getContext("2d");
canvas.width = 500;
canvas.height = 500;

const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
const analyser = audioCtx.createAnalyser();
const distortion = audioCtx.createWaveShaper();
const gainNode = audioCtx.createGain();
const source = audioCtx.createMediaElementSource(audio);

analyser.fftSize = 256;

source.connect(analyser);
analyser.connect(gainNode)
gainNode.connect(audioCtx.destination)

const bufferLength = analyser.frequencyBinCount;
const frequencyData = new Uint8Array(bufferLength);
const timeDomainData = new Uint8Array(analyser.fftSize);

let HEIGHT = window.innerHeight;
type audioDataType = 'frequency' | 'timeDomain';
type audioGraphType = 'line' | 'bars';
let selectedDataType: audioDataType = 'frequency';
let selectedGraphType: audioGraphType = 'line';
document.onmousemove = updatePage;
draw();

function updatePage(e){
  // const CurY = (window.Event) ? e.pageY : e.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
  // gainNode.gain.value = CurY/HEIGHT;
}

function draw(){
  requestAnimationFrame(draw);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  let data: Uint8Array;
  switch(selectedDataType){
    case 'frequency':
      analyser.getByteFrequencyData(frequencyData);
      data = frequencyData;
      break;
    case 'timeDomain':
      analyser.getByteTimeDomainData(timeDomainData);
      data = timeDomainData
      break; 
  }
  switch(selectedGraphType){
    case 'line':
      drawLine(data)
    break;
    case 'bars':
      drawBar(data)
  }
}

function drawLine(data: Uint8Array){
  ctx.fillStyle = 'rgb(200, 200, 200)';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.lineWidth = 2;
  ctx.strokeStyle = 'rgb(0, 0, 0)';
  ctx.beginPath();
  const sliceWidth = canvas.width * 1.0 / bufferLength;
  let x = 0;
  
  data.forEach((value,index)=>{
    const y = getY(value);
    if(index === 0){
      ctx.moveTo(x, y);
    } else{
      ctx.lineTo(x, y);
    }
    x += sliceWidth;
  })
  const lastByte = data[data.length - 1];
  ctx.lineTo(canvas.width, getY(lastByte));
  ctx.stroke();
}
function drawBar(data: Uint8Array){
  ctx.fillStyle = 'rgb(0, 0, 0)';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  const barWidth = (canvas.width / bufferLength) * 2.5
  let x = 0;
  data.forEach(value=>{
    const barHeight = (value / 255) * canvas.height;
    ctx.fillStyle = 'rgb(' + (barHeight+100) + ',50,50)';
    ctx.fillRect(x, canvas.height - barHeight, barWidth, barHeight);
    x += barWidth + 1;
  })
}

function getY(value: number){
  return canvas.height * 0.25 +  value / 255.0 * (canvas.height * 0.5);
}

const dataTypeSelect = document.getElementById('dataType') as HTMLSelectElement;
const graphTypeSelect = document.getElementById('graphType') as HTMLSelectElement;


dataTypeSelect.addEventListener('change', ()=>{
  selectedDataType = dataTypeSelect.value as audioDataType;
})
graphTypeSelect.addEventListener('change',()=>{
  selectedGraphType = graphTypeSelect.value as audioGraphType;
})

audio.addEventListener('play',()=>{
  audioCtx.resume();
},{once:true})